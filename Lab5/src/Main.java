import com.education.ztu.Product;
import java.sql.Array;
import java.util.*;
public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        ArrayList<Product> arrayList = new ArrayList<Product>();
        ArrayDeque<Product> arrayDeque = new ArrayDeque<Product>();
        TreeSet<Product> treeSet = new TreeSet<Product>();
        HashMap<String, Product> map = new HashMap<String, Product>();

        Product p2 = null, p5 = null;


        for (int index = 0 ; index < 10 ; index++){
            Product p = new Product("Product" + index, random.nextInt(200) + random.nextDouble());
            arrayList.add(p);
            arrayDeque.add(p);
            treeSet.add(p);
            map.put(p.getName(), p);
            if(index==2){
                p2=p;
            }
            if(index==5){
                p5=p;
            }
        }
        System.out.println("---------------------------------------------------------------------");
        System.out.println("arrayList");
        for (Product p :
                arrayList) {
            System.out.println("  " +p.toString());
        }
        System.out.println();
        System.out.println("Methods");
        System.out.println("get product with index 2: " + arrayList.get(6));
        System.out.println("indexOf: " + arrayList.indexOf(p2));
        System.out.println("lastIndexOf: " + arrayList.lastIndexOf(p5));
        System.out.println("remove: " + arrayList.remove(0));
        System.out.println("set 0: " + arrayList.set(0, p2));
        System.out.println("subList: " + arrayList.subList(3, 7));
        System.out.println("contains: " + arrayList.contains(p2));
        System.out.println("isEmpty: " + arrayList.isEmpty());
        System.out.println("size: " + arrayList.size());
        System.out.println();


        System.out.println("---------------------------------------------------------------------");

        System.out.println("arrayDeque");
        for (Product p :
                arrayDeque) {
            System.out.println("  " + p.toString());
        }
        System.out.println();
        System.out.println("Methods");
        arrayDeque.push(p2);
        System.out.println("push: " + arrayDeque);
        arrayDeque.offerLast(p5);
        System.out.println("offerLast: " + arrayDeque);
        System.out.println("getFirst: " + arrayDeque.getFirst());
        System.out.println("peekLast: " + arrayDeque.peekLast());
        System.out.println("pop: " + arrayDeque.pop());
        System.out.println("removeLast: " + arrayDeque.removeLast());
        System.out.println();


        System.out.println("---------------------------------------------------------------------");

        System.out.println("treeSet");
        for (Product p :
                treeSet) {
            System.out.println("  " + p.toString());
        }

        System.out.println();
        System.out.println("Methods");
        System.out.println("first: " + treeSet.first());
        System.out.println("last: " + treeSet.last());
        System.out.println("headSet: " + treeSet.headSet(p2));
        System.out.println("tailSet: " + treeSet.tailSet(p2));
        System.out.println("ceiling: " + treeSet.ceiling(p5));
        System.out.println("floor: " + treeSet.floor(p5));
        System.out.println("pollLast: " + treeSet.pollLast());
        System.out.println();


        System.out.println("---------------------------------------------------------------------");
        System.out.println("map");
        map.forEach((key,value) ->{
            System.out.println("  " + value);
        });
        System.out.println("Methods");
        System.out.println("get Product 3: " + map.get("Product3"));
        System.out.println("put Product 5: " + map.put("Product5", p5));
        System.out.println("containsKey Product 3: " + map.containsKey("Product3"));
        System.out.println("containsValue: "+ p2 + " : " + map.containsValue(p2));
        System.out.println("size: " + map.size());
        System.out.println("remove Product 3: " + map.remove("Product3"));
        System.out.println("size: " + map.size());
        System.out.println("values: " + map.values());
        System.out.println();

        for (Map.Entry<String, Product> entry : map.entrySet()) {
            String k = entry.getKey();
            Product v = entry.getValue();
            System.out.println("Key: " + k + ", Value: " + v);
        }
        System.out.println();


        System.out.println("---------------------------------------------------------------------");
        List<Product> list = Arrays.asList(
                new Product("Product3", random.nextInt(200) + random.nextDouble()),
                new Product("Product7", random.nextInt(200) + random.nextDouble()),
                new Product("Product1", random.nextInt(200) + random.nextDouble()),
                new Product("Product2", random.nextInt(200) + random.nextDouble()));

        System.out.println("List");
        for (Product p :
                list) {
            System.out.println("  " + p.toString());
        }
        Collections.sort(list);
        System.out.println("Sorted list");
        for (Product p :
                list) {
            System.out.println("  " + p.toString());
        }

        int result = Collections.binarySearch(list, new Product("Product4", 21));
        System.out.println("Binary search " + new Product("Product4", 21) + " result = " + result);

        result = Collections.binarySearch(list, p2);
        System.out.println("Binary search " + p2 + " result = " + result);

        System.out.println("max: " +Collections.max(list));
        System.out.println("min: " +Collections.min(list));
        Collections.reverse(list);
        System.out.println("reverse: " + list);
        Collections.rotate(list, 2);
        System.out.println("rotate: " + list);
    }
}