package com.education.ztu.game;
import java.util.Comparator;
public class AgeComparator implements Comparator<Participant>{
    @Override
    public int compare(Participant firstParticipant, Participant secondParticipant) {
        if(firstParticipant.getAge()> secondParticipant.getAge())
            return 1;
        else if(firstParticipant.getAge() < secondParticipant.getAge())
            return -1;
        else
            return 0;
    }
}
