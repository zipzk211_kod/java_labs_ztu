package com.education.ztu.game;
import java.util.Comparator;

public class NameComparator implements Comparator<Participant>{
    @Override
    public int compare(Participant firstParticipant, Participant secondParticipant) {
        return firstParticipant.getName().compareTo(secondParticipant.getName());
    }
}
