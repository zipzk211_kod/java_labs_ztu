package com.education.ztu;
import com.education.ztu.game.*;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        System.out.println("First part");
        Schoolar firstSchoolar = new Schoolar("Olexandra", 20);
        Student firstStudent = new Student("Mike", 19);
        Employee firstEmployee = new Employee("Vova", 25);

        Team<Schoolar> scollarTeam = new Team<Schoolar>("Team21");
        scollarTeam.addNewParticipant(firstSchoolar);

            try {
                Schoolar secondSchoolar = (Schoolar)firstSchoolar.clone();
                Student secondStudent = (Student)firstStudent.clone();
                Employee secondEmployee = (Employee)firstEmployee.clone();
                System.out.println();

                System.out.println("firstSchoolar");
                System.out.println(firstSchoolar.toString());
                System.out.println(firstSchoolar.hashCode());
                System.out.println();

                System.out.println("secondSchoolar clone firstSchoolar");
                System.out.println(secondSchoolar.toString());
                System.out.println(secondSchoolar.hashCode());
                System.out.println();

                System.out.println("firstStudent");
                System.out.println(firstStudent.toString());
                System.out.println(firstStudent.hashCode());
                System.out.println();

                System.out.println("secondStudent clone firstStudent");
                System.out.println(secondStudent.toString());
                System.out.println(secondStudent.hashCode());
                System.out.println();

                System.out.println("firstEmployee");
                System.out.println(firstEmployee.toString());
                System.out.println(firstEmployee.hashCode());
                System.out.println();

                System.out.println("secondEmployee clone firstEmployee");
                System.out.println(secondEmployee.toString());
                System.out.println(secondEmployee.hashCode());
                System.out.println();

                Team cloneTeam = scollarTeam.clone();
                System.out.println("scollarTeam");
                System.out.println(scollarTeam.toString());
                System.out.println(scollarTeam.hashCode());
                System.out.println();

                System.out.println("cloneTeam clone scollarTeam");
                System.out.println(cloneTeam.toString());
                System.out.println(cloneTeam.hashCode());


            }catch (CloneNotSupportedException exception){
                System.out.println("An error occurred during cloning");
            }
        System.out.println("-----------------------------------------------------------------");
        System.out.println("Second part");
        Schoolar first = new Schoolar("Alex", 21);
        Schoolar second = new Schoolar("Tery", 18);
        Schoolar third = new Schoolar("Olga", 22);

        Collection<Participant> crew = new ArrayList<Participant>();
        crew.add(first);
        crew.add(second);
        crew.add(third);

        AgeComparator ageComparator = new AgeComparator();

        TreeSet<Participant> ageCompare = new TreeSet<Participant>(ageComparator);
        ageCompare.addAll(crew);

        Comparator<Participant> comparator = new NameComparator().thenComparing(new AgeComparator());
        TreeSet<Participant> nameAgeCompare = new TreeSet<Participant>(comparator);
        nameAgeCompare.addAll(crew);

        System.out.println("Initial list");
        for (Participant participant :
                crew) {
            System.out.println(participant);
        }

        System.out.println();
        System.out.println("Age comparison");

        for (Participant participant :
                ageCompare) {
            System.out.println(participant);
        }
        System.out.println();
        System.out.println("Comparison of age and names");

        for (Participant participant :
                nameAgeCompare) {
            System.out.println(participant);
        }
    }
}
