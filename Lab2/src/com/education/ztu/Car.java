package com.education.ztu;

public class Car {
    String brand;
    Engine engine;

    Car(String brand){
        this.brand=brand;
    }

    class Engine{
        boolean engineWorks;
        void startEngine(){
            this.engineWorks=true;
        }
        void stopEngine(){
            this.engineWorks=false;
        }
    }

}
