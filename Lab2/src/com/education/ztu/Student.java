package com.education.ztu;

public class Student extends Person {
    private int course;
    private String speciality;
    private String fullInfo;
    private String university;
    private static int counter;

    Student(String speciality,String university,int course){
        super();
        this.fullInfo=getFullInfo();
        this.university=university;
        this.course=course;
        this.speciality= speciality;
        counter++;
    }

    Student(String firstName,String lastName, int age, Gender gender,Location location,String speciality,String university,int course){
        super(firstName, lastName, age, gender, location );
        this.fullInfo=getFullInfo();
        this.university=university;
        this.course=course;
        this.speciality= speciality;
        counter++;
    }

    @Override
    void getOccupation() {
        System.out.println("Study");
    }
    @Override
    public void whoIAm() {
        System.out.println("Student");
    }
    static int showCounter(){
        return counter;
    }

}
