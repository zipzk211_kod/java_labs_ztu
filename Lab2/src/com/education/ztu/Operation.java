package com.education.ztu;


public class Operation {
    public static int addition(int ...args){
        int sum = 0;
        for (int value : args) {
            sum += value;
        }
        return sum;
    }
    public static int subtraction(int ...args){
        int odds = 0;
        for (int value : args) {
            odds -= value;
        }
        return odds;
    }
    public static int multiplication(int ...args){
        int mult = 0;
        for (int value : args) {
            mult *= value;
        }
        return mult;
    }
    public static double division(int ...args){
        int div = 0;
        for (int value : args) {
            div /= value;
        }
        return div;
    }
    public static double average(int ...args){
        int sum = 0;
        for (int i = 0; i < args.length; i++)
            sum += args[i];

        return  ((double) sum) / args.length;
    }
    public static int maximum(int ...args){
        int i;
        int max = args[0];
        for (i = 1; i < args.length; i++)
            if (args[i] > max)
                max = args[i];

        return max;
    }
    public static int minimum(int ...args){
        int i;
        int min = args[0];
        for (i = 1; i < args.length; i++)
            if (args[i] < min)
                min = args[i];

        return min;
    }

    public static void print(Location ...args){
        for(Location arg: args){
            System.out.println(arg);
        }

    }
}
