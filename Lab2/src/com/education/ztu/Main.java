package com.education.ztu;

public class Main {
    public static void main(String[] args) {
        Car BMW = new Car("BMW");
        Car Mercedes = new Car("Mercedes");
        Teacher teacher = new Teacher("Alla", "Gertast", 32, Gender.FEMALE, Location.ZHYTOMER, "ZTU", BMW, "Math");
        Teacher teacher2 = new Teacher("Ana", "Lipets", 30, Gender.FEMALE, Location.KYIV, "KNU", Mercedes, "Math");
        System.out.println("First teacher");
        System.out.println(teacher.getFullInfo());
        teacher.whoIAm();
        System.out.println("Second teacher");
        teacher2.sayName();
        teacher2.sayLocation();
        teacher2.sayGender();
        System.out.println("Teacher`s counter");
        System.out.println(Teacher.showCounter());

        Student Alex = new Student("Software engineer", "ZTU", 3);
        System.out.println("First student");
        System.out.println(Alex.getFullInfo());
        Alex.sayAge();
        Student Ann = new Student("Ann", "Lelyah", 19, Gender.FEMALE, Location.VINNYTSYA, "Software engineer", "VNTU", 2);
        System.out.println("Second student");
        System.out.println(Ann.getFullInfo());
        Ann.sayLocation();
        System.out.println("Student`s counter");
        System.out.println(Student.showCounter());

        Employee Accountant = new Employee("ZTU", "accountant", Mercedes);
        System.out.println("Employee");
        System.out.println(Accountant.getFullInfo());
        Accountant.whoIAm();
        System.out.println(Employee.showCounter());

        System.out.println("instanceof");
        System.out.println("Teacher - " + (teacher instanceof Teacher));
        System.out.println("Student - " + (Alex instanceof Student));
        System.out.println("Operation(Location)");
        Operation.print(Location.values());
    }
}