package com.education.ztu;

public abstract class Person implements Human {
    int age;
    String firstName;
    String lastName;
    String fullInfo;
    Gender gender;
    Location location;

    Person(){
        this.firstName="Olexandra";
        this.lastName="Krosnik";
        this.fullInfo=this.firstName+" "+this.lastName;
        this.age=20;
    }

    Person(String firstName,String lastName,int age,Gender gender,Location location){
        this.firstName=firstName;
        this.lastName=lastName;
        this.fullInfo=this.firstName+" "+this.lastName;
        this.age=age;
        this.gender=gender;
        this.location=location;
    }
    public String getFullInfo(){
        return this.fullInfo;
    }
    public String getFirstName(){
        return  this.firstName;
    }
    public String getLastName(){
        return this.lastName;
    }
    public Gender getGender(){
        return this.gender;
    }
    public  Location getLocation(){
        return  this.location;
    }
    public void setAge(int age){
        this.age=age;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public  void setLastName(String lastName){
        this.lastName= lastName;
    }
    public void setGender(Gender gender){
        this.gender = gender;
    }
    public  void setLocation(Location location){
        this.location = location;
    }
    abstract void getOccupation();
    @Override
    public void sayAge(){
        System.out.println("Age- "+this.age);
    };
    @Override
    public void sayGender(){
        System.out.println("Gender- "+this.gender);
    };
    @Override
    public void sayLocation(){
        System.out.println("Location- "+this.location);
    };
    @Override
    public void sayName(){
        System.out.println("Name- "+this.firstName);
    };

}
