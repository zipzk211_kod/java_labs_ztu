package com.education.ztu;

public class Teacher extends Person{
    private Car car;
    private String fullInfo;
    private String subject;
    private String university;
    private static int counter;

    Teacher(String fullInfo,String university,Car car){
        super();
        this.fullInfo=getFullInfo();
        this.university=university;
        this.car=car;
        counter++;
    }

    Teacher(String firstName,String lastName, int age, Gender gender,Location location, String university,Car car, String subject){
        super(firstName, lastName, age, gender, location );
        this.fullInfo=getFullInfo();
        this.university=university;
        this.car=car;
        this.subject=subject;
        counter++;
    }

    @Override
    void getOccupation() {
        System.out.println("Teach");
    }
    @Override
    public void whoIAm() {
        System.out.println("Teacher");
    }
    public static int showCounter(){
        return counter;
    }

}
