package com.education.ztu;

public interface Human {
    String fullInfo = "";

    void sayAge();
    void sayGender();
    void sayLocation();
    void sayName();
    default void  whoIAm(){
        sayName();
    }
}
