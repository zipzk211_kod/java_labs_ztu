package com.education.ztu;

public class Employee extends Person{
    private String fullInfo;
    private Car car;
    private String company;
    private String position;
    private static int counter;

    Employee(String company, String position, Car car){
        super();
        this.fullInfo=getFullInfo();
        this.company=company;
        this.car=car;
        this.position=position;
        counter++;
    }

    Employee(String firstName,String lastName, int age, Gender gender,Location location,String company, String position, Car car){
        super(firstName, lastName, age, gender, location );
        this.fullInfo=getFullInfo();
        this.position=position;
        this.company=company;
        this.car=car;
        counter++;
    }

    @Override
    void getOccupation() {
        System.out.println("Work");
    }
    @Override
    public void whoIAm() {
        System.out.println("Employee");
    }
    static int showCounter(){
        return counter;
    }

}
