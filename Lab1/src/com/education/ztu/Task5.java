package com.education.ztu;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        Scanner number = new Scanner(System.in);

        System.out.println("Enter number:");

        int inputNumber = number.nextInt();

        if(inputNumber<0) {
            System.out.println("A negative number: " + inputNumber);
            return;
        }

        int sum =0;

        for(int n=inputNumber; n!=0; n/=10){
            sum +=(n%10);
        }

        System.out.println("Number: "+inputNumber);
        System.out.println("Sum: "+sum);

        number.close();
    }
}
