package com.education.ztu;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    static int[] fibonacci(int number) {
        int[] arr = new int[number];
        arr[0] = 0;
        arr[1] = 1;
        for (int i = 2; i < arr.length; ++i) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
        return arr;
    }

     static int[] reverse(int[] arr) {
        int left = 0;
        int right = arr.length - 1;

        while( left < right ) {
            int temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left++;
            right--;
        }
        return arr;
    }

    public static void main(String[] args) {
        Scanner lengthInput = new Scanner(System.in);

        System.out.println("Enter the number of elements in the array:");

        int inputNumber = lengthInput.nextInt();

        int[] arrNumbers = fibonacci(inputNumber);
        System.out.println("Array:  "+Arrays.toString(arrNumbers));

        int[] arrReverse = reverse(arrNumbers);
        System.out.println("Reverse array:  "+Arrays.toString(arrReverse));

    }
}
