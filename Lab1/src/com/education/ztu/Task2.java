package com.education.ztu;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner numbers = new Scanner(System.in);

        System.out.println("Enter two numbers separated by a space:");

        int firstNumber = numbers.nextInt();
        int secondNumber = numbers.nextInt();

        System.out.println("First number: " + firstNumber);
        System.out.println("Second number: " + secondNumber);
        System.out.println("The sum of two numbers: " + (firstNumber +secondNumber));
        numbers.close();
    }
}
