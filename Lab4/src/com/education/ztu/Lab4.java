package com.education.ztu;
import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.education.ztu.exeption.WrongLoginException;
import com.education.ztu.exeption.WrongPasswordException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.LocalDate;

public class Lab4 {
    public static void first(String str){
        System.out.println("1. The last character of the string - "+str.toCharArray()[str.length()-1]);
        if (str.endsWith("!!!"))
            System.out.println("The line ends with '!!!'");
        if (str.startsWith("I learn"))
            System.out.println("The line starts at 'I learn'");
        if (str.contains("Java"))
            System.out.println("The line has 'Java'");
        System.out.println("5. 'Java' position = "+str.lastIndexOf("Java"));
        System.out.println("6. Replace all characters 'a' with 'o' - "+ str.replace("a","o"));
        System.out.println("7. Upper case? - "+str.toUpperCase());
        System.out.println("8. Lower case? - "+str.toLowerCase());
        System.out.println("9. Remove 'Java' - " + str.substring(8,12));

    }
    public static void second(){
        int a = 4;
        int b = 36;
        StringBuilder builder1 = new StringBuilder(a +" + "+b);
        StringBuilder builder2 = new StringBuilder(a +" - "+b);
        StringBuilder builder3 = new StringBuilder(a +" * "+b);
        builder1.append(" = "+ (a+b));
        builder2.append(" = "+ (a-b));
        builder3.append(" = "+ (a*b));
        System.out.println(builder1);
        System.out.println(builder2);
        System.out.println(builder3);
        builder1.deleteCharAt(7);
        builder1.insert(7, "equal");
        builder2.replace(7,8, "equal");
        builder3.reverse();
        System.out.println(builder1);
        System.out.println(builder2);
        System.out.println(builder3);
        System.out.println("Length - "+builder1.length());
        System.out.println("Capacity - "+builder1.capacity());

    }

    public static void third(){
        Formatter order = new Formatter();
        order.format("Date and time of purchase: %25s%n", "01.12.2022 21:58:01");
        order.format("====================================================%n");
        order.format("%-3s %-15s %-15s %10s%n","№","Product","Category","Price");
        order.format("====================================================%n");
        order.format("%-3s %-15s %-15s %12.2f UAH%n","1.","Jeans","Woman wear",1500.78);
        order.format("%-3s %-15s %-15s %12.2f UAH%n","2.","Skirt","Woman wear",1000.56);
        order.format("%-3s %-15s %-15s %12.2f UAH%n","3.","Tie","Man wear",500.78);
        order.format("====================================================%n");
        order.format("All cost: %38s UAH",3002.34);
        System.out.println(order);

    }

    public static boolean fourth(String login,String password,String confirmPassword){
        String regex = "[A-Za-z][A-Za-z\\d_]{1,20}$";

        Pattern p = Pattern.compile(regex);
        Matcher mLogin = p.matcher(login);
        Matcher mPassword = p.matcher(password);

       try{
           if(!mLogin.matches()){
               throw new WrongLoginException();
           }
           if(!mPassword.matches() || password!=confirmPassword){
               throw new WrongPasswordException();
           }
           return true;
       }catch(Exception e){
           System.out.println(e.toString());
           return false;
       }
    }

    public static void fifth(){
        LocalDateTime now = LocalDateTime.of(2022,12,02,19,57,02);
        System.out.println("Start time of task 6: " + now);
        System.out.println("Weekday: " + now.get(ChronoField.DAY_OF_WEEK));
        System.out.println("Day of the year: " + now.get(ChronoField.DAY_OF_YEAR));
        System.out.println("Month: " + now.get(ChronoField.MONTH_OF_YEAR));
        System.out.println("Year: " + now.get(ChronoField.YEAR));
        System.out.println("Hours: " + now.get(ChronoField.HOUR_OF_DAY));
        System.out.println("Minutes: " + now.get(ChronoField.MINUTE_OF_DAY));
        System.out.println("Seconds: " + now.get(ChronoField.SECOND_OF_DAY));
        System.out.println(now.toLocalDate().isLeapYear() ? "Leap year " : "Not a leap year");
        System.out.println("Current time: " + LocalDate.now());
        System.out.println("IsAfter: " + LocalDate.now().isAfter(now.toLocalDate()));
        System.out.println("IsBefore: " + LocalDate.now().isBefore(now.toLocalDate()));
        System.out.println("Changed date: " + now.withMonth(12));


    }




    public static void main(String[] args){
        System.out.println("Task 1");
        first("I learn Java!!!");
        System.out.println("\n");

        System.out.println("Task 2");
        second();
        System.out.println("\n");

        System.out.println("Task 3");
        third();
        System.out.println("\n");

        System.out.println("Task 4");
        boolean f = fourth("Alexandra1","alex","alex");
        System.out.println(f);
        System.out.println("\n");

        System.out.println("Task 5");
        fifth();


    }
}
